<?php

namespace Drupal\webform_deter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form to configure Webform Deter settings.
 */
class WebformDeterSettingsForm extends ConfigFormBase {

  /**
   * The default warning message to be used when none can be retrieved.
   */
  const DEFAULT_MESSAGE = 'It appears that you may have sensitive information in your submission. Please do not submit information like social security numbers, dates of birth, etc using this form. If there is no sensitive information in your submission click OK to continue. Otherwise, click Cancel to have the opportunity to remove this information and then resubmit.';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_deter_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['webform_deter.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('webform_deter.settings');

    $form['warning_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#description' => $this->t('The message shown to users when a deter pattern matches.'),
      '#default_value' => $config->get('warning_message') ?? self::DEFAULT_MESSAGE,
    ];

    $form['patterns'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Patterns'),
      '#description' => $this->t("A new-line separated list of patterns to match against."),
      '#default_value' => implode("\r\n", $config->get('patterns') ?: []),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();
    $config = $this->config('webform_deter.settings');

    $config->set('warning_message', $form_state->getValue('warning_message'));

    $patterns = explode("\n", $form_state->getValue('patterns'));
    $patterns = array_map('trim', $patterns);
    $patterns = array_filter($patterns, 'strlen');
    $config->set('patterns', $patterns);
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
