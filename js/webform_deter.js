/**
 * @file
 * File webform_deter.js.
 */

(function (Drupal, drupalSettings, once) {
  Drupal.behaviors.webform_deter = {
    log_clear : function() {
      if (drupalSettings.webform_deter.debug) {
        if (console && console.log) {
          console.clear();
        }
      }
    },
    log : function(msg) {
      if (drupalSettings.webform_deter.debug) {
        if (console && console.log) {
          console.log(msg);
        }
      }
    },
    check_matches : function(event) {
      Drupal.behaviors.webform_deter.log_clear();
      var allowSubmission = true;
      this.querySelectorAll('input[type="text"], textarea').forEach(function (element) {
        var i, p;
        for (i in drupalSettings.webform_deter.patterns) {
          p = new RegExp(drupalSettings.webform_deter.patterns[i], 'i');
          Drupal.behaviors.webform_deter.log('Checking: ' + element.id + ' against ' + i + ': ' + p);
          if (element.value.match(p)) {
            Drupal.behaviors.webform_deter.log(element.id + ' matched');
            allowSubmission = false;
          }
        }
      });

      var ret = (allowSubmission) ? allowSubmission : window.confirm(drupalSettings.webform_deter.warning_message);
      if (!drupalSettings.webform_deter.debug) {
        if (!ret) {
          event.preventDefault();
          // Stop it one time but remove the event listener,
          // so they can still submit when/if they make corrections.
          // We're only aiming to deter!
          Drupal.behaviors.webform_deter.remove_check_matches(this);
        }
      }
      else {
        Drupal.behaviors.webform_deter.log('Processing complete');
        event.preventDefault();
      }
    },
    remove_check_matches : function(element) {
      element.removeEventListener('submit', Drupal.behaviors.webform_deter.check_matches);
      element.addEventListener('submit', Drupal.behaviors.webform_deter.submit_form);
    },
    submit_form : function() {
      this.submit();
    },
    attach: function(context) {
      if (drupalSettings.webform_deter.warning_message.length > 1) {
        var webform = once('webform-deter', 'form[class*="webform"]', context).shift();
        if (webform) {
          webform.addEventListener('submit', Drupal.behaviors.webform_deter.check_matches);
        }
      }
    }
  };
})(Drupal, drupalSettings, once)
