Webform Deter
---------------

### About this Module

This module allows for configuration of regular expressions that will be
checked against text and textarea elements on webforms and caution the user
against submitting sensitive information if the regular expressions match.

### Configuration

Go to `/admin/config/system/webform_deter/settings` to configure this module.

#### Define the Warning Message

The message you define here will be displayed to users that might have entered
sensitive information in their submissions. Note that, depending on the patterns
you define, it is possible to have false-positives, so it is probably a good
idea to word this message in a way that makes it clear what happens.

#### Define the RegEx patterns

Enter one patter per line, to be chacked in all text input webform fields.

Here are some examples:

```
(dob|birthday|date of birth) -> A text indicating DOB might be present
[\\d \\-]{13,19} -> Credit Card formats
(card number|credit card) -> A text indicating Credit Card might be present
\\d{1,2}[^\\d]\\d{1,2}[^\\d]\\d{2,4} -> A date of birth pattern (## ## ####)
\\d{9} -> A driver's license format (9 consecutive digits)
(license number|driver\'s license number) -> A text indicating DL might exist
[\\d\\-]{9,11} -> Social Security Number patterh
(ssn|social security number) -> Text indicating SSN might be there
```
