<?php

namespace Drupal\Tests\webform_deter\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\webform_deter\Form\WebformDeterSettingsForm;

/**
 * Tests basic webform_deter functionality.
 *
 * @package Drupal\Tests\webform_deter\FunctionalJavascript
 *
 * @group webform_deter
 */
class IntegrationTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'webform',
    'webform_deter',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $account = $this->drupalCreateUser([
      'access webform overview',
      'administer webform',
      'administer webform submission',
      'bypass node access',
      'view any webform submission',
      'edit any webform submission',
      'administer webform_deter',
      'access administration pages',
    ]);
    $this->drupalLogin($account);
  }

  /**
   *  Tests basic webform deter functionality.
   */
  public function testIntegration() {
    $session = $this->getSession();
    $page = $session->getPage();
    $assert_session = $this->assertSession();

    // Before anything else, check that the install defined a default message
    // and no patterns.
    $config = \Drupal::config('webform_deter.settings');
    $message = $config->get('warning_message');
    $this->assertEquals(WebformDeterSettingsForm::DEFAULT_MESSAGE, $message);
    $patterns = $config->get('patterns');
    $this->assertEquals([], $patterns);

    // Configure something else on the config form.
    $this->drupalGet('/admin/config/system');
    $page->clickLink('Webform Deter Settings');
    $assert_session->addressEquals('/admin/config/system/webform_deter/settings');
    $assert_session->pageTextContains('Webform Deter Settings');
    $assert_session->pageTextContains('The message shown to users when a deter pattern matches.');
    $assert_session->pageTextContains('A new-line separated list of patterns to match against.');
    $page->fillField('warning_message', 'Hey ho, private info in here!');
    $patterns = <<<PATTERNS
(dob|birthday|date of birth)
[\\d \\-]{13,19}
(card number|credit card)
\\d{1,2}[^\\d]\\d{1,2}[^\\d]\\d{2,4}
\\d{9}
(license number|driver\'s license number)
[\\d\\-]{9,11}
(ssn|social security number)
PATTERNS;
    $page->fillField('patterns', $patterns);
    $page->findButton('Save configuration')->press();
    $assert_session->pageTextContains('The configuration options have been saved');

    $this->drupalGet('/webform/contact');
    $assert_session->fieldExists('Your Name');
    $page->fillField('subject', 'Foo');
    // Test a bunch of matching text samples.
    $personal_info = [
      'dob',
      'birthday',
      'date of birth',
      'card number',
      'credit card',
      '4545 4545 1212 1212',
      '123456789',
      'license number',
      '123-45-6789',
      'ssn',
      'social security number',
    ];
    foreach ($personal_info as $value) {
      $page->fillField('message', 'aaa ' . $value . ' bbb');
      $page->findButton('Send message')->press();
      $this->assertTrue($this->assertAlertMessage('Hey ho, private info in here!'));
      $this->cancelAlert();
    }
    // If you click "OK" in the alert, the webform submission goes through.
    $page->fillField('message', 'aaa ssn bbb');
    $page->findButton('Send message')->press();
    $this->assertTrue($this->assertAlertMessage('Hey ho, private info in here!'));
    $this->confirmAlert();
    $assert_session->pageTextContains('Your message has been sent');
  }

  /**
   * Whether there is a JS alert open with the given text.
   *
   * @param string $text
   *   The text that should contain the alert message.
   *
   * @return bool
   *   TRUE if there is an active JS alert with the given text, FALSE otherwise.
   */
  protected function assertAlertMessage($text) {
    return $text === $this->getSession()->getDriver()->getWebDriverSession()->getAlert_text();
  }

  /**
   * Click "OK" in the confirmation / alert dialog.
   */
  protected function confirmAlert() {
    $this->getSession()->getDriver()->getWebDriverSession()->accept_alert();
  }

  /**
   * Click "Cancel" to dismiss the confirmation / alert dialog.
   */
  protected function cancelAlert() {
    $this->getSession()->getDriver()->getWebDriverSession()->dismiss_alert();
  }

}
